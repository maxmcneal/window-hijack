<div align="center">
    <h1>
        <img src="images/header.png" alt="WindowHijack">
    </h1>
    <h4>
        Intuitive window hijacking solution demonstrating <a href="https://github.com/ocornut/imgui/" target="_blank">ImGUI</a> and <a href="https://www.overwolf.com/" target="_blank">Overwolf</a>.
    </h4>
</div>

<div align="center">
  <img src="https://img.shields.io/github/v/release/nhn/tui.editor.svg?include_prereleases">
  <img src="https://img.shields.io/github/license/nhn/tui.editor.svg">
  <img src="https://img.shields.io/badge/%3C%2F%3E%20with%20%E2%99%A5-ff1414.svg">
  <img src="https://img.shields.io/badge/$-donate-ff69b4.svg?maxAge=2592000&amp;style=flat">
</div>

<div align="center">
  <a href="#-goals-and-requirements">Goals and Requirements</a> •
  <a href="#-key-learnings">Key Learnings</a> •
  <a href="#-installation">Installation</a> •
  <a href="#-contributing">Contributing</a> •
  <a href="#-credits">Credits</a> •
  <a href="#-license">License</a>
</div>

<br/>

![screenshot](images/example.gif)


## 🎯 Goals and Requirements
- Explore overwolf's overlay framework
- Preserving native window flags 
- Intuitively acquire keyboard and mouse inputs

## 📙 Key learnings
- ImGUI
- DirectX11 
- SetWindowsHookEx 

## ⚙️ Installation
- TBA

## 🐛 Contributing
- TBA

## 💭 Credits
- me 
- ImGUI ( ocornut and contributors )

## 📜 License

This project is licensed under the terms of the MIT license and protected by Udacity Honor Code and Community Code of Conduct. See <a href="LICENSE.md">license</a> and <a href="LICENSE.DISCLAIMER.md">disclaimer</a>.
