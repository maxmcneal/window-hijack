#ifndef _OVERLAY_HPP
#define _OVERLAY_HPP

// TODO: Cleanup
class Overlay
{
private:

	HWND handle;
	ID3D11Device* d3d_device;
	ID3D11DeviceContext* device_context;
	IDXGISwapChain* swap_chain;
	ID3D11RenderTargetView* render_target_view;
    
	ImDrawList* draw_list;

	int	screen_w, screen_h;

public:

	bool device_initiate();
	void device_destroy();

	void imgui_initiate();
	void imgui_destroy();
	void render_target_initiate();
	void render_target_destroy();

	void window_destroy();

	const void render();

	void handle_input();
	bool msg_loop();

	Overlay(HWND target_window);
	~Overlay();
};

#endif // _OVERLAY_HPP