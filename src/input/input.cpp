#include "input.hpp"

LRESULT CALLBACK Input::kb_callback(int n_code, WPARAM w_param, LPARAM l_param)
{
    if (n_code >= 0)
    {
        if (w_param == WM_KEYDOWN)
        {
            KBDLLHOOKSTRUCT* keyboard_struct = reinterpret_cast<KBDLLHOOKSTRUCT*>(l_param);

            switch (keyboard_struct->vkCode)
            {
                case VK_INSERT:
                {
                    input_manager.toggle_menu = !input_manager.toggle_menu;
                    break;
                }
            }
        }
    }
    return CallNextHookEx(NULL, n_code, w_param, l_param);
}

LRESULT CALLBACK Input::mouse_callback(int n_code, WPARAM w_param, LPARAM l_param)
{
    if (n_code >= 0)
    {
        MOUSEHOOKSTRUCT* mouse_struct = (MOUSEHOOKSTRUCT*)l_param;

        switch (w_param)
        {
            case WM_LBUTTONDOWN:
            {
                input_manager.left_click = !input_manager.left_click;
                break;
            }
            case WM_LBUTTONUP:
            {
                /* ensure that it's 100% set back to false */
                input_manager.left_click = false;
                break;
            }
            case WM_RBUTTONDOWN:
            {
                input_manager.right_click = !input_manager.right_click;
                break;
            }
            case WM_RBUTTONUP:
            {
                /* ensure that it's 100% set back to false */
                input_manager.right_click = false;
                break;
            }
        }
        input_manager.mouse_position = ImVec2(mouse_struct->pt.x, mouse_struct->pt.y);
    }
    return CallNextHookEx(NULL, n_code, w_param, l_param);
}

void Input::hook_set()
{
    static bool hooked = false;

    if (!hooked) 
    {
        if (!(kb_hook = SetWindowsHookExA(WH_KEYBOARD_LL, kb_callback, NULL, 0)))
            ExceptionManger::Error("Failed WH_KEYBOARD_LL");

        if (!(mouse_hook = SetWindowsHookExA(WH_MOUSE_LL, mouse_callback, NULL, 0)))
            ExceptionManger::Error("Failed WH_MOUSE_LL");
        
        hooked = true;
    }
}

void Input::hook_release()
{
    UnhookWindowsHookEx(kb_hook);
    UnhookWindowsHookEx(mouse_hook);
}