#ifndef _INPUT_HPP
#define _INPUT_HPP

class InputManager
{
public:

	bool toggle_menu, left_click, right_click;
	ImVec2 mouse_position;

} input_manager;

class Input
{
private:
	HHOOK kb_hook;
	HHOOK mouse_hook;

public:

	static LRESULT CALLBACK kb_callback(int nCode,  WPARAM wParam, LPARAM lParam);
	static LRESULT CALLBACK mouse_callback(int nCode,  WPARAM wParam, LPARAM lParam);

	void hook_set();
	void hook_release();
};

#endif // _INPUT_HPP