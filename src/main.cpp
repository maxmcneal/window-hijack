// overlay-hijack, v0.1.0
// (main code and documentation)

// first-time users attempting to utilize solution "out-of-box"
// need to install Overwolf app
// 
// https://download.overwolf.com/install/Download?Channel=web_dl_btn


// overlay MUST be enabled in-game to function

#include <iostream>
#include <comdef.h>
#include <corecrt.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <windows.h>
#include <stdio.h>
#include <dwmapi.h> 
#include <d2d1.h>
#include <dwrite.h>
#include <d3d9types.h>
#include <d3d11.h>
#include <tchar.h>
#include <cstdlib>
#include <mutex>
#include <string>

int main()
{
    // attempt to find overwatch window
    HWND target_window = FindWindowA("OOPO_WINDOWS_CLASS", "ow overlay");
    if (!target_window) {
        throw std::runtime_error("Failed to find Overwatch Overlay window.");
    }

    // instantiate new overlay
    Overlay overlay(target_window);

    // run overlay msg loop
    if (!overlay.msg_loop()) {
        throw std::runtime_error("Render failed.");
    }

    
    return 0;
}